Simple Lazyloader is a module, which provides integration of jQuery Lazyloader
(https://github.com/tuupola/jquery_lazyload) with some basic options such as:
- activate/deactivate the lazy load;
- apply lazy load to all images;
- apply lazy load to specific content types, custom block types, views,
webforms, taxonomy terms, etc.;
Requirements

* jQuery Lazyload (https://github.com/tuupola/jquery_lazyload/archive/1.x.zip).
- asset path /libraries/jquery_lazyload/jquery.lazyload.min.js
Installation

Install the module as usual, more info can be found on:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-mod...
How to use

Go to: Administration -> Configuration -> Under Media tab go to Simple Lazyloader,
adjust your configuration and save it.
