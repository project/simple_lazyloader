/**
 * @file
 * Lazyload.
 */

(function ($) {
  'use strict';
  $('img.lazy').lazyload({
    effect: 'fadeIn'
  });

})(jQuery);
